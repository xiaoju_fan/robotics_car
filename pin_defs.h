#ifndef PINS_H
#define PINS_H

// pin definitions

// vibrator (speaker)
const int pin_vibrator = 10;

// car
const int pwm_pin_forward = 5;
const int pwm_pin_backward = 6;
const int pin_left = 7;
const int pin_right = 8;
const int pin_light_1 = 2;

const int speed_sensor = 3;

const int voltage_sensor = A0;

// IRemote
const int pin_ir = 13;

// sonar
const int pin_echo = 11;
const int pin_trigger = 12;

#endif
