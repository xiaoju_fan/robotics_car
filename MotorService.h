#ifndef MOTORSERVICE_H
#define MOTORSERVICE_H

#include "TaskManager.h"
#include "car_actions.h"

#include "BrakeTask.h"

class CMotorService : public ITask
{
public:
  CMotorService() : ITask(10), m_preferred_voltage(0), m_direction(car::param_stop), m_steering(car::param_straight) {}
  
  virtual void run()
  {
    if (TM.GetActiveTask() == &BrakeTask) return;

    if (m_direction == car::param_stop) {
      car::action_go(car::param_stop, 0);
      return;
    }
  
    int preferred_pwm = (m_preferred_voltage / car::read_voltage()) * 255;
    if (preferred_pwm > 255) preferred_pwm = 255;
    car::action_go(m_direction, preferred_pwm);
  }
  
  void go(int direction, float voltage = 0)
  {
    m_direction = direction;
    m_preferred_voltage = voltage;
    run();
  }
  
  void turn(int steering) 
  {
    m_steering = steering;
    car::action_turn(steering); 
  }
  
  int GetDirection()
  {
    return m_direction;
  }
  
  int GetSteering()
  {
    return m_steering;
  }
  
private:
  float m_preferred_voltage;
  int m_direction;
  int m_steering;
  
};

extern CMotorService MotorService;

#endif
