#ifndef _SOUNDS_h
#define _SOUNDS_h

#include "pitches.h"

const unsigned int snd_start[] = { NOTE_C4, 8, NOTE_E4, 8, NOTE_G4, 8, NOTE_C5, 4};

const unsigned int snd_bear1[] = { NOTE_A3, 8, NOTE_D4, 8, NOTE_D4, 8, NOTE_E4, 8, NOTE_FS4, 8, NOTE_FS4, 16, NOTE_G4, 16, NOTE_A4, 8, NOTE_G4, 8, NOTE_FS4, 8, NOTE_FS4, 8, NOTE_E4, 8, NOTE_E4, 8, NOTE_D4, 4 };
const unsigned int snd_bear2[] = { NOTE_A4, 16, NOTE_G4, 16, NOTE_FS4, 8, NOTE_FS4, 8, NOTE_FS4, 8, NOTE_G4, 16, NOTE_FS4, 16, NOTE_E4, 8, NOTE_E4, 8, NOTE_E4, 8};
const unsigned int snd_bear3[] = { NOTE_A4, 8, NOTE_FS4, 8, NOTE_D4, 8, NOTE_E4, 8, NOTE_E4, 8, NOTE_D4, 4};

const unsigned int snd_no_command[] = { NOTE_E4, 8, NOTE_C4, 8};

const unsigned int snd_shell_on[] = { NOTE_C4, 8, NOTE_D4, 8, NOTE_E4, 8, NOTE_F4, 8, NOTE_G4, 8};
const unsigned int snd_shell_off[] = { NOTE_G4, 8, NOTE_F4, 8, NOTE_E4, 8, NOTE_D4, 8, NOTE_C4, 8};

#endif
