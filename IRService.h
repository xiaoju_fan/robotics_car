#ifndef IRSERVICE_H
#define IRSERVICE_H

#include "TaskManager.h"
extern CTaskManager TM;

#include "EAGET_RC_defs.h"
#include "sounds.h"

#include "MotorService.h"
#include "AITask.h"
#include "SimpleShell.h"
#include "BrakeTask.h"

class CIRService : public ITask
{
public:
  CIRService() : m_bInputingNumber(false) {}
  
  virtual void initialize() 
  {
    irrecv.enableIRIn(); // Start the receiver
  }
  
  virtual void activate()
  {
    car::rc_off();
  }

  virtual void run()
  {
    decode_results results;
    if (irrecv.decode(&results)) {
      if (results.decode_type == NEC && results.value != 0xFFFFFFFF) {
        switch (results.value)
        {
        case KEY_UP:
          if (MotorService.GetDirection() == car::param_backward) MotorService.go(car::param_stop);
          else MotorService.go(car::param_forward, 3.3);
          break;
        case KEY_DOWN:
          if (MotorService.GetDirection() == car::car::param_forward) MotorService.go(car::param_stop);
          else MotorService.go(car::param_backward, 2.2);
          break;
        case KEY_LEFT:
          if (MotorService.GetSteering() == car::param_right) MotorService.turn(car::param_straight);
          else MotorService.turn(car::param_left);
          break;
        case KEY_RIGHT:
          if (MotorService.GetSteering() == car::param_left) MotorService.turn(car::param_straight);
          else MotorService.turn(car::param_right);
          break;
        case KEY_OK:
          MotorService.go(car::param_stop);
          MotorService.turn(car::param_straight);
          break;
        case KEY_POWER:
          if (TM.GetActiveTask() != &SimpleShell)
          {
            play_music(snd_shell_on, sizeof(snd_shell_on));
            TM.RemoveTask(&MotorService);
            TM.RemoveTask(&AITask);
            TM.ActivateTask(&SimpleShell);
          }
          else 
          {
            play_music(snd_shell_off, sizeof(snd_shell_off));
            TM.AddTask(&MotorService);
            TM.AddTask(&AITask);
            TM.ActivateTask(this);
          }
          break;
        case KEY_1:
          TM.ActivateTask(&AITask);
          break;
        case KEY_2:
          play_music(snd_bear1, sizeof(snd_bear1));
          break;
        case KEY_3:
          play_music(snd_bear2, sizeof(snd_bear2));
          break;
        case KEY_4:
          play_music(snd_bear3, sizeof(snd_bear3));
          break;
        case KEY_MENU:
          MotorService.go(car::param_stop);
          MotorService.turn(car::param_straight);
          TM.ActivateTask(&BrakeTask);
          break;
        default:
          play_music(snd_no_command, sizeof(snd_no_command));
          Serial.print("Undefined key received: ");
          Serial.println(results.value, HEX);
          break;
        }
      }
      irrecv.resume(); // Receive the next value
    }
  }
  
private:
  bool m_bInputingNumber;
  
  static int convert_key_to_number(const unsigned long &value)
  {
    switch (value)
    {
    case KEY_1:
      return 1;
    case KEY_2:
      return 2;
    case KEY_3:
      return 3;
    case KEY_4:
      return 4;
    case KEY_5:
      return 5;
    case KEY_6:
      return 6;
    case KEY_7:
      return 7;
    case KEY_8:
      return 8;
    case KEY_9:
      return 9;
    case KEY_0:
      return 0;
    default:
      return -1;
    }
  }
};

extern CIRService IRService;

#endif

