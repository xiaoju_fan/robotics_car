#include <Arduino.h>
#include <NewTone.h>
#include <IRremote.h>
#include "misc.h"
#include "pin_defs.h"

#include "DebugOutput.h"
CDebugOutput g_DebugOutput;

volatile unsigned long g_rounds = 0;
volatile unsigned long g_last_rounds_change = 0;

NewPing sonar(pin_trigger, pin_echo, 500);
IRrecv irrecv(pin_ir);

int read_number(char **buf, int *len)
{
  int value = 0;
  while (*len > 0 && **buf <= '9' && **buf >= '0') {
    value = value * 10 + **buf - '0';
    *buf += 1;
    *len -= 1;
  }
  *buf += 1;
  *len -= 1;
  return value;
}

void play_music(const unsigned int *buf, int len)
{
  for (int i = 0; i < len / sizeof(buf[0]) / 2; i++) {
    int noteLength = 1000 / buf[i * 2 + 1];
    NewTone(pin_vibrator, buf[i * 2], noteLength);
    delay(noteLength * 1.30);
    noNewTone(pin_vibrator);
  }
}

