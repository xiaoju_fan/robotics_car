#ifndef DEBUGOUTPUT_H
#define DEBUGOUTPUT_H

enum DEBUG_LEVEL {
  NoDebug = 0,
  Error = 1, 
  Warning = 2,
  Information = 3,
};

class CDebugOutput
{
public:
  CDebugOutput() : m_DebugLevel(NoDebug) {}
  
  void SetDebugLevel(DEBUG_LEVEL DebugLevel) { m_DebugLevel = DebugLevel; }
  DEBUG_LEVEL GetDebugLevel() { return m_DebugLevel; }
  
  template<typename T>
  void DebugOutput(DEBUG_LEVEL DebugLevel, T Msg) 
  {
    if (m_DebugLevel >= DebugLevel) {
      Serial.print(Msg);
    }
  }
  
  template<typename T>
  void DebugOutputln(DEBUG_LEVEL DebugLevel, T Msg) 
  {
    if (m_DebugLevel >= DebugLevel) {
      Serial.println(Msg);
    }
  }
  
private:
  DEBUG_LEVEL m_DebugLevel;
};

extern CDebugOutput g_DebugOutput;

#define SET_DEBUG_LEVEL(DebugLevel) g_DebugOutput.SetDebugLevel(DebugLevel)

#define TRACEE(Msg) g_DebugOutput.DebugOutput(Error, Msg)
#define TRACEW(Msg) g_DebugOutput.DebugOutput(Warning, Msg)
#define TRACEI(Msg) g_DebugOutput.DebugOutput(Information, Msg)

#define TRACEELN(Msg) g_DebugOutput.DebugOutputln(Error, Msg)
#define TRACEWLN(Msg) g_DebugOutput.DebugOutputln(Warning, Msg)
#define TRACEILN(Msg) g_DebugOutput.DebugOutputln(Information, Msg)

#endif
