#ifndef AITASK_H
#define AITASK_H

#include "TaskManager.h"
extern CTaskManager TM;

#include "misc.h"

#include <NewTone.h>

#include "MotorService.h"
extern CMotorService MotorService;

class CAITask : public ITask
{
public:
  CAITask() : ITask(200), m_bActive(false), m_Status(stopping) {}
  
  virtual void activate() 
  {
    car::rc_off();
    car::set_rounds(0);
    NewTone(pin_vibrator, 220, 1000);

    m_bActive = true; 
    m_Status = going_forward;
    MotorService.go(car::param_forward, 3.3);
  }

  virtual void deactivate() 
  { 
    m_bActive = false;
    m_Status = stopping;
    MotorService.go(car::param_stop);
  }
 
protected:
  enum { stopping, going_forward, going_backward, turning_left, turning_right };

  virtual void run() 
  {
    if (!m_bActive) return;

    int distance = sonar.convert_cm(sonar.ping_median(5));
    Serial.println(distance);
    if (distance == 0) return;

    switch (m_Status)
    {
    case stopping:
      break;
    case going_forward:
      if (distance < 110) {
        m_Status = turning_left;
        MotorService.turn(car::param_left);
      }
      break;
    case going_backward:
      break;
    case turning_left:
    if (distance > 140) {
        m_Status = stopping;
        MotorService.go(car::param_stop);
        MotorService.turn(car::param_straight);
      }
      break;
    case turning_right:
      break;
    default:
      break;
    }
  }
  
private:
  bool m_bActive;
  int m_Status;
};

extern CAITask AITask;

#endif
