#ifndef MISC_H
#define MISC_H

#include <NewPing.h>
extern NewPing sonar;

volatile extern unsigned long g_rounds;
volatile extern unsigned long g_last_rounds_change;

#include <IRremote.h>
extern IRrecv irrecv;

int read_number(char **buf, int *len);
void play_music(const unsigned int *buf, int len);

#define itemsof(array) (sizeof(array) / sizeof(array[0]))

#define BEGIN_PARSE_COMMAND(command_buf, len) \
{ \
  char **buf = command_buf; \
  int *l = len; \
  int v = read_number(buf, l); \
  if (false) {} \

#define COMMAND_HANDLER_0(verb, action) \
  else if (v == verb) { \
  action(); \
} \

#define COMMAND_HANDLER_1(verb, action) \
  else if (v == verb) { \
  int arg1 = read_number(buf, l); \
  action(arg1); \
} \

#define COMMAND_HANDLER_2(verb, action) \
  else if (v == verb) { \
  int arg1 = read_number(buf, l); \
  int arg2 = read_number(buf, l); \
  action(arg1, arg2); \
} \

#define COMMAND_HANDLER_V(verb, action) \
  else if (v == verb) { \
  action(buf, l); \
} \

#define END_PARSE_COMMAND \
  else { \
  Serial.println(F("[A] ?")); \
} \
} \

#endif
