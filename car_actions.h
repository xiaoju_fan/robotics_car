#ifndef CAR_ACTIONS_H
#define CAR_ACTIONS_H

#include <Arduino.h>
#include "car_actions.h"
#include "pin_defs.h"
#include "misc.h"
#include "DebugOutput.h"

class car
{
private:
  static void on_speed_sensor_change()
  {
    unsigned long now = millis();
    if (now != g_last_rounds_change) {
      g_rounds += 1;
      
      TRACEI("Rounds: ");
      TRACEILN(g_rounds);

      g_last_rounds_change = now; 
    }
  }

public:
  enum GO_PARAMETERS { 
    param_stop  = 0,
    param_forward = 1,
    param_backward = 2,
  };

  enum TURN_PARAMETERS {
    param_straight = 0,
    param_left = 1,
    param_right = 2,
  };

  static void initialize()
  {
    // initialize the speed sensor
    pinMode(speed_sensor, INPUT_PULLUP);
    attachInterrupt(1, on_speed_sensor_change, FALLING);
  }

  static void action_go(int direction, int power = 0)
  {
    TRACEI(F("[A] go "));
    switch (direction)
    {
    case param_stop:
      TRACEI(F("[P] stop "));
      analogWrite(pwm_pin_forward, 0);
      analogWrite(pwm_pin_backward, 0);
      break;
    case param_forward:
      TRACEI(F("[P] forward "));
      analogWrite(pwm_pin_backward, 0);
      analogWrite(pwm_pin_forward, power);
      break;
    case param_backward:
      TRACEI(F("[P] backward "));
      analogWrite(pwm_pin_forward, 0);
      analogWrite(pwm_pin_backward, power);
      break;
    default:
      TRACEI(F("[P] ?"));
    }
    TRACEILN("");
  }

  static void action_turn(int steering)
  {
    TRACEI(F("[A] turn "));
    switch (steering)
    {
    case param_straight:
      TRACEI(F("[P] straight "));
      digitalWrite(pin_left, LOW);
      digitalWrite(pin_right, LOW);
      break;
    case param_left:
      TRACEI(F("[P] left "));
      digitalWrite(pin_right, LOW);
      digitalWrite(pin_left, HIGH);
      break;
    case param_right:
      TRACEI(F("[P] right "));
      digitalWrite(pin_left, LOW);
      digitalWrite(pin_right, HIGH);
      break;
    default:
      TRACEI(F("[P] ?"));
    }
    TRACEILN("");
  }

  static void rc_off()
  {
    pinMode(pwm_pin_forward, OUTPUT);
    pinMode(pwm_pin_backward, OUTPUT);
    pinMode(pin_left, OUTPUT);
    pinMode(pin_right, OUTPUT);
    TRACEILN(F("RC: off"));
  }

  static void rc_on()
  {
    pinMode(pwm_pin_forward, INPUT);
    pinMode(pwm_pin_backward, INPUT);
    pinMode(pin_left, INPUT);
    pinMode(pin_right, INPUT);
    TRACEILN(F("RC: on"));
  }
  
  static void set_rounds(unsigned long rounds) { g_rounds = rounds; }
  static unsigned long get_rounds() { return g_rounds; }
  
  static float read_voltage()
  {
    const float r1 = 6.8;
    const float r2 = 10;
    int sensorValue = analogRead(voltage_sensor);
    return 3.3 / 1023 * float(sensorValue) / (r1 / (r1 + r2) );
  }
};

#endif
