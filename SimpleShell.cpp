#include <Arduino.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <NewPing.h>
#include <NewTone.h>
#include "SimpleShell.h"
#include "pin_defs.h"
#include "misc.h"
#include "car_actions.h"
#include "pitches.h"
#include "sounds.h"
#include "DebugOutput.h"

enum {
  verb_go = 1,
  verb_turn = 2,
  verb_ping = 3,
  verb_read_speed_sensor = 4,
  verb_output_sound = 5,
  verb_get_rounds = 6,
  verb_output_voltage = 7,
  verb_toggle_output = 8,
  verb_reset = 999,
};

void CSimpleShell::activate() 
{
  car::rc_off();
  car::action_turn(car::param_straight);
  car::action_go(car::param_stop, 0);
  delay(500);
  car::rc_on();
}

static int current_way = car::param_stop;
static int current_where = car::param_straight;

void CSimpleShell::car_action_go_wrapper(int way, int power)
{
  car::rc_off();
  car::action_go(way, power);
  current_way = way;
  if (current_way == car::param_stop && current_where == car::param_straight) {
    delay(500);  
    car::rc_on();
  }
}

void CSimpleShell::car_action_turn_wrapper(int where)
{
  car::rc_off();
  car::action_turn(where);
  current_where = where;
  if (current_way == car::param_stop && current_where == car::param_straight) {
    delay(500);
    car::rc_on();
  }
}

void (* resetFunc)(void) = 0;

void CSimpleShell::test_speed_sensor()
{
  Serial.println(digitalRead(speed_sensor));
}

void CSimpleShell::test_voltage_sensor()
{
  const float r1 = 6.8;
  const float r2 = 10;
  int sensorValue = analogRead(voltage_sensor);
  float voltage = 3.3 / 1023 * float(sensorValue) / (r1 / (r1 + r2) );
  Serial.println(sensorValue);
  Serial.println(voltage);
}

void CSimpleShell::get_rounds()
{
  Serial.println(car::get_rounds());
}

void CSimpleShell::output_sound(char **buf, int *len)
{
  int note = read_number(buf, len);
  int duration = read_number(buf, len);
  while (duration != 0) 
  {
    int noteLength = 1000 / duration;
    NewTone(pin_vibrator, note, noteLength);
    delay(noteLength * 1.30);
    noNewTone(pin_vibrator);
    note = read_number(buf, len);
    duration = read_number(buf, len);
  }
}

void CSimpleShell::test_sonar()
{
  Serial.print("Ping: ");
  Serial.print(sonar.ping_cm()); // Print result (0 = outside set distance range, no ping echo)
  Serial.println("cm");
}

void CSimpleShell::toggle_output()
{
  if (g_DebugOutput.GetDebugLevel() != NoDebug) g_DebugOutput.SetDebugLevel(NoDebug);
  else g_DebugOutput.SetDebugLevel(Information);
}

void CSimpleShell::run()
{
  if (Serial.available() > 0) {
    char command[64]; 
    char *command_buf = command;
    int len = Serial.readBytesUntil(10, command, sizeof(command));  
    BEGIN_PARSE_COMMAND(&command_buf, &len)
    COMMAND_HANDLER_2(verb_go, car_action_go_wrapper)
    COMMAND_HANDLER_1(verb_turn, car_action_turn_wrapper)
    COMMAND_HANDLER_0(verb_ping, test_sonar)
    COMMAND_HANDLER_0(verb_read_speed_sensor, test_speed_sensor)
    COMMAND_HANDLER_V(verb_output_sound, output_sound)
    COMMAND_HANDLER_0(verb_get_rounds, get_rounds)
    COMMAND_HANDLER_0(verb_output_voltage, test_voltage_sensor)
    COMMAND_HANDLER_0(verb_toggle_output, toggle_output)
    COMMAND_HANDLER_0(verb_reset, resetFunc)
    END_PARSE_COMMAND
  }
}
