#ifndef EAGET_RC_DEFS_H
#define EAGET_RC_DEFS_H

#define KEY_POWER 0xBD10EF
#define KEY_MUTE 0xBD807F

#define KEY_1 0xBDA857
#define KEY_2 0xBD6897
#define KEY_3 0xBDE817
#define KEY_4 0xBD9867
#define KEY_5 0xBD58A7
#define KEY_6 0xBDD827
#define KEY_7 0xBDB847
#define KEY_8 0xBD7887
#define KEY_9 0xBDF807
#define KEY_0 0xBD827D

#define KEY_UP 0xBDD02F
#define KEY_DOWN 0xBDF00F
#define KEY_LEFT 0xBD926D
#define KEY_RIGHT 0xBD52AD
#define KEY_OK 0xBDB04F
#define KEY_MENU 0xBD50AF
#define KEY_EXIT 0xBDE01F

#endif
