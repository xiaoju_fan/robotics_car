#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include "misc.h"

class ITask
{
public:
  ITask(unsigned long TimeSlice = 0) : m_TimeSlice(TimeSlice), m_NextRun(0) {}
  
  void schedule()
  {
    // This number will overflow (go back to zero), after approximately 50 days.
    unsigned long now = millis();
    if (now > m_NextRun) {
      run();
      m_NextRun = now + m_TimeSlice;
    }
  }

  virtual void initialize() {};
  virtual void activate() {};
  virtual void deactivate() {};
 
protected:
  virtual void run() = 0;
  
private:
  unsigned long m_NextRun;
  unsigned long m_TimeSlice;
};

class CTaskManager
{
public:
  CTaskManager() : m_pActiveTask(NULL) { for (int i = 0; i < itemsof(m_TaskList); i++)  m_TaskList[i] = NULL; }
  
  void AddTask(ITask *pTask)
  {
    for (int i = 0; i < itemsof(m_TaskList); i++) {
      if (m_TaskList[i] == pTask) return;
    }
    
    for (int i = 0; i < itemsof(m_TaskList); i++) {
      if (m_TaskList[i] == NULL) {
        m_TaskList[i] = pTask;
        break;
      } 
    }
  }
  
  void RemoveTask(ITask *pTask) 
  {
    for (int i = 0; i < itemsof(m_TaskList); i++) {
      if (m_TaskList[i] == pTask) {
        m_TaskList[i] = NULL;
        break;
      } 
    }
  }
  
  void run() 
  {
    for (int i = 0; i < itemsof(m_TaskList); i++) {
      if (m_TaskList[i] != NULL) {
        m_TaskList[i]->schedule();
      } 
    }
  }
  
  void initialize() 
  {
    for (int i = 0; i < itemsof(m_TaskList); i++) {
      if (m_TaskList[i] != NULL) {
        m_TaskList[i]->initialize();
      } 
    }
  }
  
  void ActivateTask(ITask *pTask) 
  {
    if (m_pActiveTask != NULL) m_pActiveTask->deactivate();
    m_pActiveTask = pTask;
    if (m_pActiveTask != NULL) m_pActiveTask->activate();
  }
  
  ITask* GetActiveTask()
  {
    return m_pActiveTask;
  }
  
private:
  ITask *m_TaskList[16];
  ITask *m_pActiveTask;
};

#endif
