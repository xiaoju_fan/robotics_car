#include <NewPing.h>
#include <NewTone.h>
#include <IRremote.h>

#include "TaskManager.h"
CTaskManager TM;

#include "SimpleShell.h"
CSimpleShell SimpleShell;

#include "IRService.h"
CIRService IRService;

#include "MotorService.h"
CMotorService MotorService;

#include "AITask.h"
CAITask AITask;

#include "BrakeTask.h"
CBrakeTask BrakeTask;

#include "car_actions.h"
#include "sounds.h"
#include "misc.h"

// the setup routine runs once when you press reset:
void setup() 
{
  // open the serial port at 9600 bps:
  Serial.begin(9600);

  // initialize interrupts
  car::initialize();

  TM.AddTask(&SimpleShell);
  TM.AddTask(&IRService);
  TM.AddTask(&MotorService);
  TM.AddTask(&BrakeTask);
  TM.AddTask(&AITask);

  TM.initialize();
  TM.ActivateTask(&IRService);
  
  // ready sound and ready message
  play_music(snd_start, sizeof(snd_start));
  Serial.println("System information: ");
  Serial.print("\tBuild date: ");
  Serial.print(__TIME__);
  Serial.print(", ");
  Serial.println(__DATE__);
  Serial.print("\tBattery status: ");
  Serial.print(car::read_voltage());
  Serial.println("V");
  Serial.println(F("Ready"));
}

// the loop routine runs over and over again forever:
void loop() 
{
  TM.run();
}
