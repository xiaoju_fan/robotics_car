#ifndef BRAKETASK_H
#define BRAKETASK_H

#include "TaskManager.h"
#include "car_actions.h"
#include "misc.h"

class CBrakeTask : public ITask
{
public:
  CBrakeTask() : ITask(10), m_bActive(false), m_last_distance(0) {}
  
  virtual void activate() 
  {
    m_bActive = true;
    m_last_distance = 0;
    car::action_go(car::param_backward, 168);
  }
  virtual void deactivate() 
  {
    m_bActive = false;
    car::action_go(car::param_stop);
  };

protected:
  virtual void run()
  {
    if (!m_bActive) return;
    
    int distance = sonar.convert_cm(sonar.ping_median(5));

    if (m_last_distance == 0) {
      m_last_distance = distance;
      return;
    }
    
    if (m_last_distance <= distance) TM.ActivateTask(NULL);
    else {
      m_last_distance = distance;
      // simulate ABS system
      car::action_go(car::param_stop, 0);
      delay(50);
      car::action_go(car::param_backward, 168);
    }
  }
  
private:
  bool m_bActive;
  int m_last_distance;
};

extern CBrakeTask BrakeTask;

#endif
