#ifndef SIMPLESHELL_H
#define SIMPLESHELL_H

#include "TaskManager.h"

class CSimpleShell : public ITask
{
public:
  virtual void run();
  virtual void activate();

private:
  static void car_action_go_wrapper(int way, int power);
  static void car_action_turn_wrapper(int where);

  static void test_speed_sensor();
  static void test_voltage_sensor();
  static void output_sound(char **buf, int *len);
  static void test_sonar();
  static void toggle_output();
  static void get_rounds();

};

extern CSimpleShell SimpleShell;

#endif

